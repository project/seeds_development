## Overview

The **Seeds Development Helper** module provides developers and site administrators with essential utilities to optimize and streamline Drupal development. It focuses on enhancing image style usage and content type form management by offering the following features:

- **Image Style Checker**: Identify unused image styles across the site, helping developers clean up unused assets.
- **Field Group Generator**: Automatically generate field groups for content type forms, improving the usability and organization of administrative interfaces.

---

### Image Style Checker

- Navigate to **Media** > **Image Styles** in the admin panel.  
- Click on **Unused Images** to view all unused image styles.  
- Alternatively, click on **Operations** next to a specific image style to inspect its usage.  

---

### Field Group Generator

- Go to **Structure** > **Content types** in the admin panel.  
- Select a content type and click on **Manage Form Display**.  
- Click on the **Generate Field Groups** button.  
- The module will automatically create field groups based on your form fields.  
- Review the generated field groups, adjust as needed, and save the form display. 